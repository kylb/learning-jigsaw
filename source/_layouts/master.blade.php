<!DOCTYPE html>
<html lang="pt-br">

<head>
    @section('head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    @show
    <title>
        @yield('title', "Clean Blog")
    </title>
    <!-- Bootstrap core CSS -->
    @section('style')
    <link href="{{$page->base_url}}/assets/build/css/main.css" rel="stylesheet">
    @show
</head>

<body>

    <!-- Navigation -->
    @section('navbar')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="{{$page->base_url}}/">Start Bootstrap</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{$page->base_url}}/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{$page->base_url}}/about">About</a>
                    </li>
                </ul>
            </div>
        </div>
        @show
    </nav>
    <!-- Page Header -->
    @section('header')
    <header class="masthead" style="background-image: url('assets/build/img/home-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>{{ $page->title }}</h1>
                        <span class="subheading">{{ $page->description }}</span>
                    </div>
                </div>
            </div>
        </div>
        @show
    </header>

    <!-- Main Content -->
    @section('content')
    @show
    <hr>

    <!-- Footer -->
    <footer>
        @section('footer')
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <ul class="list-inline text-center">
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
                </div>
            </div>
            @show
        </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    @section('js')
    <script src="{{$page->base_url}}/assets/build/js/main.js"></script>
    @show
</body>

</html>
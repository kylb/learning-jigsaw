<?php

return [
  'production' => false,
  'baseUrl' => '',
  'collections' => [],
  'title' => 'JigSaw do Poder',
  'seo' => [
    'title' => 'JigSaw',
  ],
  'isPair' => function ($page, $number) {
    return $number % 2 == 0 ? "Yes" : "No";
  }
];
